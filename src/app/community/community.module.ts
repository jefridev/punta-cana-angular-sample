import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommunityDetailComponent } from './community-detail/community-detail.component';
import { CommunityItemComponent } from './community-item/community-item.component';

@NgModule({
  declarations: [CommunityDetailComponent, CommunityItemComponent],
  exports: [CommunityDetailComponent, CommunityItemComponent],
  imports: [
    CommonModule
  ]
})
export class CommunityModule { }
