import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ScrollerService } from '../scroller.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit, AfterViewInit  {

  constructor(private scrollerService: ScrollerService) { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    try {
      const element = document.querySelector('.about-main');
      this.scrollerService.scrollToElement(element);
    } catch (e) {
    }
  }
}
