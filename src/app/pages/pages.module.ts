import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactComponent } from './contact/contact.component';
import { AboutComponent } from './about/about.component';
import { HomeComponent } from './home/home.component';
import { ScrollerService } from './scroller.service';
import { ProfileComponent } from '../profile/profile.component';
import { CommunityModule } from '../community/community.module';
import { CommunityComponent } from './community/community.component';
import { PropertyModule } from '../property/property.module';

@NgModule({
  declarations: [ContactComponent, AboutComponent, HomeComponent, ProfileComponent, CommunityComponent],
  exports: [ContactComponent],
  providers: [ScrollerService],
  imports: [
    CommonModule,
    CommunityModule,
    PropertyModule
  ]
})
export class PagesModule { }
