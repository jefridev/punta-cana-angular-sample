import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ScrollerService } from '../scroller.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit, AfterViewInit {
  constructor(private scrollerService: ScrollerService) {}

  ngOnInit() {}

  ngAfterViewInit(): void {
    try {
      const element = document.querySelector('.contact-service-box');
      this.scrollerService.scrollToElement(element);
    } catch (e) {}
  }
}
