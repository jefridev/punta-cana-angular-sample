import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ScrollerService } from '../scroller.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, AfterViewInit {

  constructor(private scrollerService: ScrollerService) { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    try {
      // const element = document.querySelector('.home-main');
      // this.scrollerService.scrollToElement(element);
    } catch (e) {
    }
  }
}
