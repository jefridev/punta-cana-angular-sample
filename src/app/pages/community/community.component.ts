import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ScrollerService } from '../scroller.service';

@Component({
  selector: 'app-community',
  templateUrl: './community.component.html',
  styleUrls: ['./community.component.css']
})
export class CommunityComponent implements OnInit, AfterViewInit {

  constructor(private scrollerService: ScrollerService) { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    try {
      const element = document.querySelector('.community-main');
      this.scrollerService.scrollToElement(element);
    } catch (e) {
    }
  }
}
