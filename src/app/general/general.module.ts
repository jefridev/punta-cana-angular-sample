import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterComponent } from './footer/footer.component';
import { TopNavbarComponent } from './top-navbar/top-navbar.component';
import { BannerBoxComponent } from './banner-box/banner-box.component';
import { RouterModule } from '@angular/router';
import { MapBoxComponent } from './map-box/map-box.component';

@NgModule({
  declarations: [FooterComponent, TopNavbarComponent, BannerBoxComponent, MapBoxComponent],
  exports: [FooterComponent, TopNavbarComponent, BannerBoxComponent],
  imports: [
    RouterModule,
    CommonModule
  ]
})
export class GeneralModule { }
