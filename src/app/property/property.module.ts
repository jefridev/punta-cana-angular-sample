import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PropertyItemComponent } from './property-item/property-item.component';
import { PropertyDetailComponent } from './property-detail/property-detail.component';
import { PropertyStatusComponent } from './property-status/property-status.component';

@NgModule({
  declarations: [PropertyItemComponent, PropertyDetailComponent, PropertyStatusComponent],
  exports: [PropertyItemComponent],
  imports: [
    CommonModule
  ]
})
export class PropertyModule { }
